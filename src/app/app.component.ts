import { Component } from '@angular/core';
import {NavigationStart, Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'greenwashing';
  isSearchScreen = true;
  constructor(private router: Router) {
  }

  ngOnInit() {
    this.router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        this.isSearchScreen = event.url === '/'
      }
    });
  }

  backHome() {
    this.router.navigate(['/'])
  }
}
