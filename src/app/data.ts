export const COMPANIES = [
  {
    identifiers: ['DANONE', '11111111'],
    name: 'Danone',
    sector: 'Agroalimentaire',
    logo: 'assets/danone.png',
    score: '8',
    numberOfNews: 40,
    numberOfClimateNews: 24,
    numberOfClimateNewsWithMateriality: 11,
    numberOfSocialAction: 12,
    greenwashing: 13,
    // sector
    sectorNumberOfNews: 56,
    sectorNumberOfClimateNews: 12,
    sectorNumberOfClimateNewsWithMateriality: 2,
    sectorNumberOfSocialAction: 1,
    sectorGreenwashing: 10,

    keywords: ['Lait végétal', 'Énergie Solaire'],
    lastEvents: [
      {
        title: 'Actions Climat',
        content: 'Gaz à effets de serre, consommation d’eau, utilisation des sols'
      },
      {
        title: 'Actions Sociétales / de solidarité',
        content: 'Dons aux associations, projets de reforestation, actions pour la biodiversité'
      },
      {
        title: 'Communication',
        content: 'Greenwashing, ou communication normale des actions'
      }
    ]
  }
]
