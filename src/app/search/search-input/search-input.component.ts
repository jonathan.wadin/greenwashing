import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss']
})
export class SearchInputComponent implements OnInit {
  siren: string = '';

  constructor(private router: Router) {
  }

  moveToCompany() {
    this.router.navigate(['/company'], {queryParams: {'id': this.siren}})
    this.siren = '';
  }

  ngOnInit(): void {
  }

}
