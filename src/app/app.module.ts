import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgChartsModule } from 'ng2-charts';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  NbThemeModule,
  NbLayoutModule,
  NbInputModule,
  NbCardModule,
  NbFormFieldModule,
  NbIconModule, NbBadgeModule, NbTagModule
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { SearchComponent } from './search/search.component';
import {FormsModule} from "@angular/forms";
import { CompanyComponent } from './company/company.component';
import { SearchInputComponent } from './search/search-input/search-input.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchComponent,
    CompanyComponent,
    SearchInputComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({name: 'default'}),
    NbLayoutModule,
    NbEvaIconsModule,
    NbInputModule,
    NbCardModule,
    NbFormFieldModule,
    NbIconModule,
    FormsModule,
    NgChartsModule,
    NbBadgeModule,
    NbTagModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
