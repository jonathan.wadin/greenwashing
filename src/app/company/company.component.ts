import {Component, OnInit} from '@angular/core';
import {COMPANIES} from "../data";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {filter} from "rxjs";
import {ChartConfiguration, ChartData, ChartType} from 'chart.js';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

  companies = COMPANIES
  selectedCompany: any;
  public radarChartOptions: ChartConfiguration['options'] = {
    responsive: true,
  };
  public radarChartLabels: string[] = ['Publication', 'Publication Climat', 'Action Climat', 'Action sociétale', 'Greenwashing'];

  public radarChartData!: ChartData<'radar'>;
  public radarChartType: ChartType = 'radar';


  constructor(private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    let id = this.route?.snapshot?.queryParams['id'].trim();
    let match = this.companies.filter(c => c.identifiers.includes(id));
    if (match.length > 0) {
      this.selectCompany(match[0]);
    } else {
      this.selectCompany(undefined);
    }
    this.router.events
      .pipe(filter((e) => e instanceof NavigationEnd))
      .subscribe((_) => {
        let id = this.route?.snapshot?.queryParams['id'].trim();
        let match = this.companies.filter(c => c.identifiers.includes(id));
        if (match.length > 0) {
          this.selectCompany(match[0]);
        } else {
          this.selectCompany(undefined);
        }
      });
  }

  private selectCompany(company: any) {
    this.selectedCompany = company;
    if (this.selectedCompany) {
      this.radarChartData = {
        labels: this.radarChartLabels,
        datasets: [
          {
            data: [company.numberOfNews, company.numberOfClimateNews, company.numberOfClimateNewsWithMateriality,
              company.numberOfSocialAction, company.greenwashing
            ], label: company.name
          },
          {
            data: [company.sectorNumberOfNews, company.sectorNumberOfClimateNews, company.sectorNumberOfClimateNewsWithMateriality,
              company.sectorNumberOfSocialAction, company.greenwashing], label: company.sector
          }
        ]
      };
    }
  }
}
